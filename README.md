# Commentator

Commentator позволяет отделить код от комментариев.
При запуске скрипта save.js все однострочные комментарии, которые начинаются с `///` будут перемещены в соответствуюший JSON файл в дирректории `.git`.

**Использование**<br>
Допустим мы находимся в дирректории `~/my_project` c файлом `~/my_project/file.js`, а commentator находится в дирректории `~/commentator`.<br>
*Сохранение комментариев:*
`node ../commentator/save.js ./file.js`
>  Важно! Чтобы сохранить комментарии, на первой строке должно быть написано `/// Commentator ON`

*Отображение комментариев:*
`node ../commentator/show.js ./file.js`<br>
На данный момент если комментариев для файла нет, то будет ошибка.
>  При этом важно помнить в какой дирректории ты находишься, чтобы правильно прописывать пути к файлам

Видео пояснение: https://youtu.be/_nb2_jYq-a4<br>


**Пример куска из .bashrc**
`
commentator() {  
    if [ "${1}" = 'save' ]; then  
        node ~/Documents/commentator/save.js "${2}"  
    elif  [ "${1}" = 'show' ]; then  
        node ~/Documents/commentator/show.js "${2}"  
    else  
        echo "Доступны две команды: commentator [save | show] [file]"  
    fi  
}
`