const fs = require('fs');
const Path = require('path');

const constants = require('./constants');

/**
 * Отображает комментарии в файле
 * @param {string} path 
 * @param {string} file 
 */
function showFileComments(path, file) {
    /// Нормализуем путь до файла
    const filePath = Path.normalize(`${path}/${file}`);
    /// Получаем нормализованный путь до файла комментариев
    const commentsFile = Path.normalize(`${constants.SUB_DIR}/${file}.json`);

    /// Читаем файл с комментариями
    console.log(`Пытаемся показать комментарии из файла: ${commentsFile}`);
    fs.readFile(commentsFile, 'utf8', (err, text) => {
        if (err) new Error();

        /// Парсим JSON
        const comments = JSON.parse(text);

        /// Читаем файл с кодом
        fs.readFile(filePath, 'utf-8', (err, text) => {
            if (err) new Error();

            /// Разбиваем файл с кодом на строки
            const lines = text.split(constants.EOL);
            /// Количество строк в файле
            const linesCount = lines.length;
            /// Количество комментариев
            const commentsCount = Object.keys(comments).length;
            /// Количество строк кода, которое получится после вставки комментариев
            const codeLength = linesCount + commentsCount;

            /// Весь код, который будет записан в файл
            let code = '';
            /// Итератор цикла (номер строки кода)
            let i = 0;

            while (i < codeLength) {
                /// Если на этой строке есть комментарий
                if (comments[i + 1]) {
                    /// Добавляем его к коду
                    code += `${comments[i + 1]}${constants.EOL}`;

                /// Если на этой строке нет комментариев
                } else if (lines.length != 0) {
                    /// И строки кода еще не закончились, то
                    /// вставляем следующую строку в код
                    code += `${lines.shift()}${constants.EOL}`;
                }

                /// Переходим к следующей строке
                i += 1;
            }

            /// Убираем перенос строки в конце кода
            lastEOLIndex = code.lastIndexOf(constants.EOL);
            code = code.slice(0, lastEOLIndex);

            /// Записываем результат в исходный файл
            fs.writeFile(filePath, code, (err) => {
                if (err) throw err;

                console.log("Ошибка: " + err);
                console.log(`Отобразил комментарии для файла ${filePath}`)
            });
        })
    })
}

/**
 * Показывает комментарии в файле
 */
function show() {
    //. Аргументы командной строки
    const args = process.argv.slice(2);

    //. Если есть аргументы
    if (args.length > 0) {
        /// первый аргумент - это путь до файла
        showFileComments(constants.WORK_DIR, args[0]);
        return
    } else { /// Если аргументов нет, то сообщаем об ошибке
        console.log('Пожалуйста, укажите, файл, для которого надо отобразить комментарии');
    }
}
show();