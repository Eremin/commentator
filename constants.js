module.exports.SINGLE_LINE_COMMENT = '///';

module.exports.WORK_DIR = process.cwd();
module.exports.GIT_DIR = `${this.WORK_DIR}/.git`;
module.exports.DIR = `${this.GIT_DIR}/docs`;
module.exports.SUB_DIR = `${this.DIR}/comments`;

module.exports.PLATFORMS = {
    WIN32: 'win32'
}
/// Если платформа win32, то конец строки это '\r\n'
module.exports.EOL = (process.platform === this.PLATFORMS.WIN32 ? '\r\n' : '\n');