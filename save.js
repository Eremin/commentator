const fs = require('fs');
const Path = require('path');

const constants = require('./constants');

/**
 * Вернет true если строка является односрочным комментарием
 * @param {string} string 
 */
function isSingleLineComment(string) {
    return string.indexOf(constants.SINGLE_LINE_COMMENT) === 0;
}

/**
 * Сохраняем комментарии для файла
 * @param {string} path
 * @param {string} file
 */
function saveFileComments(path, file) {
    /// Нормализуем путь до файла
    const filePath = Path.normalize(`${path}/${file}`);
    /// Получаем нормализованный путь до файла комментариев
    const commentsFile = Path.normalize(`${constants.SUB_DIR}/${file}.json`);

    /// Асинхронное чтение файла
    fs.readFile(filePath, 'utf8', (err, text) => {
        if (err) new Error();

        /// Файл разбивает на строки
        const lines = text.split(constants.EOL);
        /// Объект комментариев
        let comments = {};
        /// Код, который останется в файле
        let code = '';

        /// Первая строка файла должна говорить о том, что комментарии показаны
        if (lines[0] != `/// Commentator ON`) {
            /// Просто так не разрешаем сохранять комментарии
            console.log('Для сохранения, пожалуйста, включите отображение комментариев.');
            console.log('Т.е. первая строка файла должна быть такой:')
            console.log('/// Commentator ON')
            console.log('В данный момент первая строка такая:')
            console.log(lines[0]);
            console.log('-'.repeat(30));
            return // Ничего не делаем и выдаем сообщение об ошибке
        }

        /// Проходимся по всем строкам
        lines.forEach((line, i) => {
            /// Убираем лишние пробелы по краям
            const trimLine = line.trim();
            /// Проверяем является ли строка однострочным комментарием
            let isComment = isSingleLineComment(trimLine);

            /// Если является, то отправляем ее в объект комментариев
            if (isComment) {
                comments[i + 1] = line;
            } else {
                /// Если не является, то оставляем в файле
                code += `${line}${constants.EOL}`;
            }
        });

        /// Убираем перенос строки в конце кода
        lastEOLIndex = code.lastIndexOf(constants.EOL);
        code = code.slice(0, lastEOLIndex);

        /// Оставляем в исходном файле только код
        if (code.length > 0) {
            fs.writeFileSync(filePath, code, (err) => {
                if (err) new Error();
            });
        }

        /// Если есть комментарии
        if (comments) {
            /// Преобразуем объект в строку JSON
            const commentsJSON = JSON.stringify(comments);

            // Если нет .git, создаем ее и другие папки
            if (!fs.existsSync(constants.GIT_DIR)) {
                fs.mkdirSync(constants.GIT_DIR);
            }

            if (!fs.existsSync(constants.DIR)) {
                fs.mkdirSync(constants.DIR);
            }

            if (!fs.existsSync(constants.SUB_DIR)) {
                fs.mkdirSync(constants.SUB_DIR);
            }

            /// Записываем комментарии в файл //////////////////////////////

            /// Получаем путь до файла
            const commentsFilePath = Path.dirname(commentsFile);

            /// Если путь не существует
            if (!fs.existsSync(commentsFilePath)){
                /// Создаем его
                fs.mkdirSync(commentsFilePath, {recursive: true});
            }

            /// Записываем
            fs.writeFile(commentsFile, commentsJSON, (err) => {
                if (err) console.log(err);
                else console.log(`Комментарии успешно записаны в ${commentsFile}`);
            });
        }
    });
}

/**
 * Сохраняем комментарии в файл
 */
function save() {
    /// Аргументы командной строки
    const args = process.argv.slice(2);

    /// Если есть аргументы, то
    if (args.length > 0) {
        /// первый аргумент - это путь до файла
        saveFileComments(constants.WORK_DIR, args[0]);
    } else { /// Если аргументов нет, то сообщаем об ошибке
        console.log('Пожалуйста, укажите, файл, из которого надо сохранить комментарии');
    }
}
save();